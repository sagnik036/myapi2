# Generated by Django 3.2.5 on 2021-07-12 19:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapi', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='home',
            new_name='hero',
        ),
    ]
